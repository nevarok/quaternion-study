﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuaternionPlayground : MonoBehaviour {
    [SerializeField]
    private Transform obj;
    // Use this for initialization
    private void Start () {
//        nevarok.Quaternion q = new nevarok.Quaternion(Vector3.up, Mathf.PI);
        //Debug.Log(obj.rotation);
    }
    Vector3 cross = Vector3.left * 5;
    // Update is called once per frame
    //public int clicksCount;
    private void Update () {
        if(Input.GetMouseButtonUp(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit)) {
                
                obj.rotation = FromToRotation(Vector3.up, hit.point);
                    //new nevarok.Quaternion(new nevarok.Vector3(0, 1, 0), Mathf.PI * 0.25f).ToUnity();
//                    (new nevarok.Quaternion(new nevarok.Vector3(0, 1, 0), Mathf.PI * 0.25f)
//                    * new nevarok.Quaternion(new nevarok.Vector3(1, 0, 0), Mathf.PI * 0.25f)).ToUnity();
            }
        }

        Debug.DrawLine(Vector3.zero, cross, Color.red);
    }

    private Quaternion FromToRotation(Vector3 from, Vector3 to)
    {
        float angle = Vector3.Angle(from, to) * Mathf.Deg2Rad * 0.5f;
        to = Vector3.Cross(from, to).normalized * Mathf.Sin(angle);

        return new Quaternion(to.x, to.y, to.z, Mathf.Cos(angle));
    }

//    private bool VarifyQuaternion(Vector3 v, float angle)
//    {
//        return v
//    }
}
