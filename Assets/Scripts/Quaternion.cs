﻿using UnityEngine;

namespace nevarok
{
    public class Quaternion 
    {
        public Vector3 Vector { get; private set; } //x, y, z
        public float Scalar { get; private set; }   //w

        private Vector3 axie;
        private float angle;

        public Quaternion() {}

        public Quaternion(Vector3 rotationAxieNormilized, float rotationAngleInRadians)
        {
            // Q(w, x, y, z) = Q(cos(angle / 2), axie.x * sin(angle / 2), axie.y * sin(angle / 2), axie.z * sin(angle / 2))
            axie = rotationAxieNormilized;
            angle = rotationAngleInRadians;

            float halfAngle = angle * 0.5f;

            float sinHalfAngle = Mathf.Sin(halfAngle);
            float cosHalfAngle = Mathf.Cos(halfAngle);

            Vector = axie * sinHalfAngle;
            Scalar = cosHalfAngle;

            //Debug.Log("Valid? : " + Validate(this));
        }

        public Quaternion(float w, float x, float y, float z)
        {
            Vector = new Vector3(x, y, z);
            Scalar = w;
        }

        public UnityEngine.Quaternion ToUnity()
        {
            return new UnityEngine.Quaternion(Vector.x, Vector.y, Vector.z, Scalar);
        }

        public static Quaternion Invert(Quaternion q)
        {
            //Vector(0, 0, 1) = (0, 0, -1)
            //q(s, v) = (-s, -v)

            Vector3 xyz = -q.Vector; 
            return new Quaternion(q.Scalar, xyz.x, xyz.y, xyz.z);
        }

        public static bool Validate(Quaternion q)
        {
            //ww + xx + yy + zz == 1
            float sum = q.Vector.x * q.Vector.x + q.Vector.y * q.Vector.y + q.Vector.z * q.Vector.z + q.Scalar * q.Scalar;
            //Debug.Log("x * x + y * y + z * z + w * w = " + sum);

            return sum == 1;
        }

        public static Vector3 operator* (Quaternion a, Vector3 v)
        {
            Quaternion b = new Quaternion();
            b.Vector = v;
            b.Scalar = 0;

            Quaternion q = a * b * Invert(a);
            return q.Vector;
        }

        public static Quaternion operator* (Quaternion a, Quaternion b)
        {
            //q = (w, x, y, z)
            //q = (w + xi + yj + zk)
            //a = aW + aXi + aYj + aZk
            //b = bW + bXi + bYj + bZk

            //a * b =  aW * bW +  aW * bXi +  aW * bYj +  aW * bZk
            //      + aXi * bW + aXi * bXi + aXi * bYj + aXi * bZk
            //      + aYj * bW + aYj * bXi + aYj * bYj + aYj * bZk
            //      + aZk * bW + aZk * bXi + aZk * bYj + aZk * bZk

            //      =  aWbW +  aWbXi +  aWbYj + aWbZk
            //      + aXbWi - aXbX + aXbYij + aXbZik
            //      + aYbWj + aYbXji + aYbYjj + aYbZjk
            //      + aZbWk + aZbXki + aZbYkj - aZbZ

            //Hamilton's formula for complex numbers
            //ii = jj = kk = ijk = -1


            //Multiplication table
            //     |     |     |     |
            //     |  I  |  J  |  K  |      (IJK = -1) * I => IIJK = -I => -JK = -I => JK =  I
            //_____|_____|_____|_____|       (JK =  I) * J =>  -K =  JI => JI = -K
            //     |*****|     |     |       (JI = -K) * I =>  -J = -KI => KI =  J
            //  I  |*-1 *|  k  | -j  |       (KI =  J) * K =>  -I =  KJ => KJ = -I
            //_____|*****|_____|_____|       (KJ = -I) * J =>  -K = -IJ => IJ =  K
            //     |     |*****|     |       (IJ =  K) * I =>  -J =  IK => IK = -J
            //  J  | -k  |*-1 *|  i  |
            //_____|_____|*****|_____|
            //     |     |     |*****|
            //  K  |  j  | -i  |*-1 *|
            //_____|_____|_____|*****|

            //      = aWbW  + aWbXi + aWbYj + aWbZk
            //      + aXbWi - aXbX  + aXbYk - aXbZj
            //      + aYbWj - aYbXk - aYbY  + aYbZi
            //      + aZbWk + aZbXj - aZbYi - aZbZ

            // w = aWbW - aXbX - aYbY - aZbZ
            // x = aWbX + aXbW + aYbZ - aZbY
            // y = aWbY - aXbZ + aYbW + aZbX
            // z = aWbZ + aXbY - aYbX + aZbW

            //IMPORTANT to remember quaternions multiplication isn't commutative
            //       a * b != b * a
            // (a * b) * c == a * (b * c) 

            //90x 180y => 180y * 90x

            return new Quaternion(
                a.Scalar * b.Scalar   - a.Vector.x * b.Vector.x - a.Vector.y * b.Vector.y - a.Vector.z * b.Vector.z, // w = aWbW - aXbX - aYbY - aZbZ
                a.Scalar * b.Vector.x + a.Vector.x * b.Scalar   + a.Vector.y * b.Vector.z - a.Vector.z * b.Vector.y, // x = aWbX + aXbW + aYbZ - aZbY
                a.Scalar * b.Vector.y - a.Vector.x * b.Vector.z + a.Vector.y * b.Scalar   + a.Vector.z * b.Vector.x, // y = aWbY - aXbZ + aYbW + aZbX
                a.Scalar * b.Vector.z + a.Vector.x * b.Vector.y - a.Vector.y * b.Vector.x + a.Vector.z * b.Scalar    // z = aWbZ + aXbY - aYbX + aZbW
            );
        }
    }
}
